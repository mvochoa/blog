import fs from 'fs'
import { join } from 'path'
import matter from 'gray-matter'
import moment from 'moment'

export function pagesDirectory(dir) {
  return join(process.cwd(), "src", dir)
}

export function getSlugsFromDirectory(dir) {
  const slugs = {}
  fs.readdirSync(dir).forEach((file) => {
    const { data } = matter(fs.readFileSync(join(dir, file), 'utf8'))
    if (!data['slug']) data['slug'] = file.replace(/\.md$/, '')
    slugs[data['slug']] = file
  })

  return slugs
}

export function getBySlug(dir, slug, fields = [], pathSlugs) {
  const fullPath = join(dir, pathSlugs[slug])
  const fileContents = fs.readFileSync(fullPath, 'utf8')
  const { data, content } = matter(fileContents)
  const items = {}
  fields.forEach((field) => {
    switch (field) {
      case 'slug':
        items[field] = slug
        break
      case 'content':
        items[field] = content
        break
      case 'date':
        items[field] = -62135596800000
        if (data[field]) {
          data[field] = data[field].split('T')[0]
          items[field] = parseInt(moment(data[field], 'YYYY-MM-DD').format('x'))
        }
        break
      default:
        items[field] = data[field]
        break
    }
  })
  return items
}

export function getPageContentBySlug(
  slug,
  dir,
  fields = [],
  pathSlugs = getSlugsFromDirectory(pagesDirectory(dir))
) {
  return getBySlug(pagesDirectory(dir), slug, fields, pathSlugs)
}

export function getAllSlugPages(fields = [], dir) {
  const pathSlugs = getSlugsFromDirectory(pagesDirectory(dir))
  const pages = Object.keys(pathSlugs).map((slug) =>
    getPageContentBySlug(slug, dir, fields, pathSlugs)
  )
  return pages
}
