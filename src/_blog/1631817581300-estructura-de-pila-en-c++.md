---
slug: 'estructura-de-pila-en-c-plus-plus'
title: 'Estructura de pila en C++'
summary: 'Tutorial de como construir la estructura de una pila en C++ y usarla como una librería en otros proyectos.'
date: '2018-11-09'
tags: 'C++'
image: '/images/blog/1631817581300/thumbnail.png'
---

Se recomienda llamar al archivo donde se va a trabajar `pila.cpp`. Para crear una pila en C++ es necesario dos estructuras:

1.- La estructura para los Nodos de la pila que almacenan el valor y el puntero del siguiente valor.

```cpp
struct Node {
 public:
   int value;
   Node *next;
};
```

2.- La estructura de la Pila contiene el puntero del Nodo de la cima de la pila. También contiene los métodos de `Pop()`, `Push(int)`, `Empty()` y `Print()`. El de Print no es tan necesario ya que solo es para saber los valore que tiene la pila.

```cpp
struct Pila {
 public:
   Node *start = 0; // Cima de la pila
   int Pop();
   void Push(int);
   bool Empty();
   void Print();
};
```

El método `Empty()` de la pila, es para saber si la cima de la pila está vacía. retorna un `true` si la pila está vacía y un `false` si la pila _NO_ esta vacía.

```cpp
bool Pila::Empty() {
 return (this->start == NULL);
}
```

El método `Push(int)` de la pila, como argumento se recibe el valor a insertar.

Primero es necesario crear nuestro **Nodo** como puntero para insertar en la cima de la pila, al cual se le asigna el valor recibido en la función. En este caso lo vamos a llamar `tmp`.

```cpp
void Pila::Push(int value) {
 Node *tmp = new(Node);
 tmp->value = value;
}
```

Validamos que la cima de la pila _no_ esté vacía. A nuestro nuevo nodo le asignamos como siguiente nodo la cima de la pila. Por ultimo la nueva cima de la pila será el nuevo nodo que creamos `tmp`.

```cpp
void Pila::Push(int value) {
 // ...
 if (!this->Empty()) {
   tmp->next = this->start;
   this->start = tmp;
 }
}
```

Ahora si la pila está **vacía**, lo único que debemos hacer es que nuestro nuevo nodo `tmp` será la cima de la pila, pero antes hay asignarle que no tiene un nodo siguiente ya que es el único en la pila.

```cpp
void Pila::Push(int value) {
 // ...
 else {
   tmp->next = NULL;
   this->start = tmp;
 }
}
```

El método `Pop()` retorna el valor de la cima de la pila. Validamos que la cima de la pila _no_ esté vacía. Se guarda el valor de la cima de la pila en `value`, luego cambiamos la cima de la pila por el nodo que tenía como siguiente la cima de la pila y retornamos el `value`. En caso de que la cima de la pila estuviera **vacía** retornamos un -1.

```cpp
int Pila::Pop() {
 if (!this->Empty()) {
   int value = this->start->value;
   this->start = this->start->next;
   return value;
 }

 return -1;
}
```

Por último tenemos el método `Print()`. Para este método vamos a usar `cout` es necesario incluir la librería `iostream`. Todo el siguiente código va en la parte superior del documento.

```cpp
#include<iostream>
```

Bueno volviendo a la función `Print()`. Igual validamos que no esté vacía la cima de la pila. Se declara un nodo temporal se le asigna el nodo de la cima de la pila. Imprimimos el valor de la cima de pila y ciclamos la pila con los nodos siguientes hasta que se llega al último nodo y vamos imprimiendo sus valores. En caso de que este vacía la pila se imprime un mensaje de que la pila está vacía.

```cpp
void Pila::Print() {
 if (!this->Empty()) {
   Node *tmp = this->start;
   std::cout<<"Cima de la pila: "<<tmp->value<<std::endl;
   tmp = tmp->next;
   while (tmp != NULL) {
     std::cout<<"                 "<<tmp->value<<std::endl;
     tmp = tmp->next;
   }
 } else {
   std::cout<<"La pila esta vacía."<<std::endl;
 }
}
```

Como puedes notar ya que existe la función `Print()` va ser un requisito siempre importar la librería `iostream`.

## Creación del archivo `pila.h`

Lo más recomendable es crear un archivo `pila.h` para poder importarlo en nuestros demás programas, donde vayamos a usar pilas. El contenido de nuestro archivo `pila.h` sería el siguiente:

```cpp
#ifndef PILA_H
#define PILA_H

struct Node {
 public:
     int value;
     Node *next;
};

struct Pila {
 public:
   Node *start = 0; // Inicio de la pila
   int Pop();
   void Push(int);
   bool Empty();
   void Print();
};

#endif
```

Para el contenido de nuestro archivo `pila.cpp` solo serian los métodos de la estructura `Pila` y la importación de la librería `iostream` y la referencia a nuestro archivo `pila.h`:

```cpp
#include "pila.h"
#include<iostream>

int Pila::Pop() {
 // ...
}

void Pila::Push(int value) {
 // ...
}

bool Pila::Empty() {
 // ...
}

void Pila::Print() {
 // ...
}
```

## Probando el funcionamiento de la estructura

Creamos el archivo `main.cpp` en la misma ubicación donde esta: `pila.h` y `pila.cpp`, dentro importamos la librería `iostream` e importamos nuestro encabezado `pila.h`. En el método `main()` creamos una instancia de nuestra estructura `Pila`.

```cpp
#include<iostream>
#include "pila.h"

int main() {
 Pila p;
 p.Push(4);
 p.Push(3);
 p.Push(8);
 p.Print();
 std::cout<<std::endl<<"Salio el: "<<p.Pop()<<std::endl;

 std::cout<<std::endl<<"// Estado de la pila"<<std::endl;
 p.Push(12);
 p.Print();

 return 0;
}
```

Primero insertamos un `4`, `3` y un `8` a la pila. El estado del pila sería el siguiente:

```
Cima de la pila: 8
                3
                4
```

Se extrae el valor de la cima de la pila con el método `Pop()` lo que nos devuelve es el `8` y la pila quedaría:

```
Salio el: 8

// Estado de la pila
Cima de la pila: 3
                4
```

Ya por ultimo insertamos un `12` y estado de la pila sería:

```
Cima de la pila: 12
                3
                4
```

Para compilar el código se usa el siguiente comando:

```sh
$ g++ *.cpp -std=c++11 -o main.o
$ ./main.o
```

Se usa `-std=c++11` para que use el estándar 11 de C++. Bueno eso seria todo, espero que sea de ayuda.
