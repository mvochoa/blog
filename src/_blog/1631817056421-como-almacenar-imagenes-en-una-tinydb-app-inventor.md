---
slug: 'como-almacenar-imagenes-en-una-tinydb-app-inventor'
title: 'Como almacenar imágenes en una tinyDB | App Inventor'
summary: 'Guardar imágenes dentro una tinyDB en app inventor usando una extensión para convertir imágenes a BASE64.'
date: '2018-07-10'
tags: 'App Inventor, Base64, TinyDB'
image: '/images/blog/1631817056421/thumbnail.png'
---

![Ejemplo del funcionamiento de la app](/images/blog/1631817056421/1531260685w300.gif) Vamos a guardar una imagen dentro de una tinyDB y al iniciar la aplicación se recupere la imagen almacenada y la vamos a mostrar en el componente `Image`. Primero creamos un nuevo proyecto en app inventor, en mi caso lo llame `SaveImageTiny`. Vamos a necesitar importar una extensión para convertir imágenes a Base64, hice una publicación de como hacer la extensión ahi la puedes descargar: [Crear una extension para convertir imágenes a base64 y viceversa](/blog/como-crear-una-extension-para-convertir-imagenes-en-base64-y-viceversa-app-inventor).

Ahora creamos la interfaz de la aplicación con los siguientes componentes:

- **ImagePicker**
    - Name: BtnSelectImage
    - Width: Fill Parent
    - Text: Seleccionar Imagen
- **Label**
    - Text: Imagen almacenada
- **Image**
    - Width: Fill Parent
    - ScalePictureToFit: true
- **TinyDB**
- **Base64**: La extensión que mencionaba anteriormente.

![Interfaz de la app](/images/blog/1631817056421/1531256915.png)

En la programación de los bloques primero vamos hacer un procedimiento que lo vamos a llamar `SetImageOfTinyDB` para recuperar la imagen de la tinyDB.

![Procedure set image of tiny db](/images/blog/1631817056421/1531257108.png)

Usamos el método **Base64ToImage** del componente `Base64` para convertir el texto almacenado en el tag *image* de la `tinyDB` a una dirección de una imagen temporal para mostrarla en el componente `Image`.

En el evento **AfterPicking** del componente `ImagePicker` que se ejecuta después de que se á seleccionado una imagen hacemos lo siguiente:

![Bloque guardar imagen en la tiny db](/images/blog/1631817056421/1531257132.png)

Guardamos el valor devuelto por el método **ImageToBase64** del componente `Base64` en la tinyDB con el tag *image*, lo que necesita este método es la dirección de la imagen seleccionada que la extraemos de la variable **Selection** del componente `ImagePicker` y también llamamos al procedimiento creado anteriormente.

![Screen initialize](/images/blog/1631817056421/1531257160w200.png) Por ultimo en el evento **Initialize** del Screen llamamos al procedimiento `SetImageOfTinyDB` para que cuando inicie el screen cargue la imagen que tenemos guardada en la tinyDB.

Bueno eso seria todo te comparto los enlaces de los archivos:

- Proyecto de la app: [SaveImageTiny.aia](http://bit.ly/2KZjT6G)
- Apk de la app: [SaveImageTiny.apk](http://bit.ly/2uc3Qv6)

Espero que te haya sido de ayuda. No olvides si ha sido util para ti seria de mucha ayuda si compartes este material con tus amigos.