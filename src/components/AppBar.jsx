import Link from '@/components/Link'

export default function AppBar() {
  return (
    <>
      <header className="flex justify-between items-center container px-4 py-4 mb-6 lg:px-8 lg:py-4">
        <div>
          <Link href="/" className="cursor-pointer">
            <div className="flex justify-center items-center">
              <img
                src="/images/logo.svg"
                alt="Mvochoa"
                className="rounded-full bg-mvochoa p-1 w-12"
              />
              <span className="block pl-2 text-lg font-mono tracking-wider">Mvochoa</span>
            </div>
          </Link>
        </div>
        <div>
          <Link href="/blog" className="cursor-pointer px-2">
            Blog
          </Link>
        </div>
      </header>
    </>
  )
}
