import CardPost from '@/components/CardPost'

export default function ListPosts({ posts }) {
  return (
    <div className="flex justify-between items-start flex-wrap">
      {posts.map((post, index) => (
        <CardPost
          key={index}
          to={`/blog/${post.slug}`}
          image={post.image}
          title={post.title}
          summary={post.summary}
          date={post.date}
        />
      ))}
    </div>
  )
}
