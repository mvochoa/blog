import classnames from 'classnames'

import Link from '@/components/Link'

export default function Button({ href, className = '', ...props }) {
  return (
    <Link href={href}>
      <button
        {...props}
        className={classnames(
          'cursor-pointer text-sm font-semibold border border-gray-200 py-2 px-4 text-center rounded',
          {
            'text-blue-600': !props.disabled,
            'text-gray-400': props.disabled,
          },
          className
        )}
      />
    </Link>
  )
}
