import ReactMarkdown from 'react-markdown'
import SyntaxHighlighter from 'react-syntax-highlighter'

import Link from '@/components/Link'

function p(node, ...props) {
  return <p className="font-normal text-base my-1">{node.children}</p>
}

function strong(node, ...props) {
  return <strong className="font-bold text-base">{node.children}</strong>
}

function h1(node, ...props) {
  return (
    <>
      <h1 className="font-semibold text-2xl mt-3">{node.children}</h1>
      <hr className="mb-2 mt-1" />
    </>
  )
}

function h2(node, ...props) {
  return (
    <>
      <h2 className="font-medium text-xl mt-3">{node.children}</h2>
      <hr className="mb-2 mt-1" />
    </>
  )
}

function h3(node, ...props) {
  return (
    <>
      <h3 className="font-normal text-lg mt-3">{node.children}</h3>
      <hr className="mb-2 mt-1" />
    </>
  )
}

function ul(node, ...props) {
  return <ul className="list-disc ml-8">{node.children}</ul>
}

function ol(node, ...props) {
  return <ol className="list-decimal ml-8">{node.children}</ol>
}

function li(node, ...props) {
  return <li className="font-normal text-base">{node.children}</li>
}

function code(node, ...props) {
  return (
    <code className="font-mono text-sm whitespace-pre-wrap break-all rounded py-0.5 px-1 text-gray-700 bg-gray-100">
      {node.children}
    </code>
  )
}

function pre(node, ...props) {
  const { className = '', children } = node.children[0].props
  return (
    <SyntaxHighlighter
      className="font-mono text-sm rounded my-2 py-1 px-2 text-gray-600 bg-gray-100"
      language={className.split('-').pop().toLowerCase()}
    >
      {children[0]}
    </SyntaxHighlighter>
  )
}

function a(node, ...props) {
  return (
    <Link className="text-blue-600 cursor-pointer" href={node.href}>
      {node.children}
    </Link>
  )
}

function img(node, ...props) {
  return <img className="rounded mb-2 w-full sm:w-auto" src={node.src} alt={node.alt} />
}

export default function Markdown({ children }) {
  return (
    <ReactMarkdown
      className="markdown"
      components={{
        h1: h1,
        h2: h2,
        h3: h3,
        p: p,
        a: a,
        img: img,
        strong: strong,
        ul: ul,
        ol: ol,
        li: li,
        code: code,
        pre: pre,
      }}
    >
      {children}
    </ReactMarkdown>
  )
}
