import AppBar from './AppBar'
import Footer from './Footer'

export default function Layout({ children, className = '' }) {
  return (
    <>
      <AppBar />
      <main className={`${className}`}>{children}</main>
      <Footer />
    </>
  )
}
