import Link from '@/components/Link'

export default function Footer() {
  const social = {
    facebook: 'https://m.facebook.com/mvochoamv/',
    youtube: 'https://www.youtube.com/channel/UCDz7bc0iWUIvgG-mK1NPhDw',
    gitlab: 'https://gitlab.com/mvochoa',
  }
  return (
    <>
      <footer className="bg-gray-100 text-gray-600 text-sm flex justify-between items-center container px-8 py-4 mt-6">
        <p>© {new Date().getFullYear()} Mvochoa</p>
        <div className="flex justify-between items-center">
          {Object.keys(social).map((key, index) => (
            <Link key={index} href={social[key]} className="cursor-pointer mx-2">
              <img className="opacity-60" src={`/images/social/${key}.svg`} alt={key} />
            </Link>
          ))}
        </div>
      </footer>
    </>
  )
}
